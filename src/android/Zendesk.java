package com.c4uno.zendesk;

import android.content.Context;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONException;

import java.util.Locale;

import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.support.Support;
import zendesk.support.guide.HelpCenterActivity;

public class Zendesk extends CordovaPlugin {
  private static final String ACTION_INITIALIZE = "initialize";
  private static final String ACTION_SET_ANONYMOUS_IDENTITY = "setAnonymousIdentity";
  private static final String ACTION_SHOW_HELP_CENTER = "showHelpCenter";

  @Override
  public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext)
    throws JSONException {

    if (ACTION_INITIALIZE.equals(action)) {
      /**
       * Variables to initialize Support Instance
       */
      String appId = args.getString(0);
      String clientId = args.getString(1);
      String zendeskUrl = args.getString(2);

      /**
       * Variables to set Support Help Center Locale
       * Default is Latam Spanish
       */
      String localeLanguage = "es";
      String localeCountry = "419";

      Locale locale = new Locale(localeLanguage, localeCountry);

      zendesk.core.Zendesk.INSTANCE.init(this.getContext(), zendeskUrl, appId, clientId);
      Identity identity = new AnonymousIdentity();
      zendesk.core.Zendesk.INSTANCE.setIdentity(identity);
     Support.INSTANCE.setHelpCenterLocaleOverride(locale);
     Support.INSTANCE.init(zendesk.core.Zendesk.INSTANCE);
     } else if (ACTION_SHOW_HELP_CENTER.equals(action)) {
     HelpCenterActivity.builder().show(this.cordova.getActivity());
     } else if (ACTION_SET_ANONYMOUS_IDENTITY.equals(action)) {
     String name = args.getString(0);
     String email = args.getString(1);

     Identity identity = new AnonymousIdentity.Builder()
     .withNameIdentifier(name)
     .withEmailIdentifier(email)
     .build();

     zendesk.core.Zendesk.INSTANCE.setIdentity(identity);
     Support.INSTANCE.init(zendesk.core.Zendesk.INSTANCE);
     } else {
     callbackContext.error("Invalid action: " + action);
     return false;
     }

     callbackContext.success();
     return true;
     }

private Context getContext() {
  return this.cordova.getActivity().getApplicationContext();
  }
  }
